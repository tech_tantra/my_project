from django.conf.urls import url
from myintro import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    #/music/71/
    url(r'^(?P<education_id>[0-9]+)/$', views.detail, name='detail')
]