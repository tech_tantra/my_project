# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

#album Red pk =1
#album ..
class Education(models.Model):
    level = models.CharField(max_length=200)
    degree_title = models.CharField(max_length=200)
    pic_logo = models.CharField(max_length=500)
    country  = models.CharField(max_length=500)
    def __str__(self):
        return self.level + ' _ ' + self.degree_title

class institute(models.Model):
    education = models.ForeignKey(Education, on_delete=models.CASCADE)
    college_name = models.CharField(max_length=200)
    years_attended = models.CharField(max_length=250)
    gpa = models.CharField(max_length=500)
